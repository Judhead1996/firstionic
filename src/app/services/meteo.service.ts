import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MeteoService {

  constructor(private httpClient:HttpClient) { }

  public getMeteoData(city:string) {
    return this.httpClient.get("https://api.openweathermap.org/data/2.5/forecast?q="+city+"&appid=1481ec21894e1144d8176fc8823e95b1")

  }
}

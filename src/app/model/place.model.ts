export interface Place {
    title: string;
    city?: string; // '?' veut dire k c'est pas obligatoire c'est obtionnel
    country?: string;
    keyWords?: string;
    selected?: boolean;
    timestamp?: number;
    coordinates?: {
        longitude: number;
        latitude: number;
    };
    photos?: string[];
}
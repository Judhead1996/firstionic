import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public contact = {
    name: "KHADANE",
    email: "inuyashakha96@gmail.com",
    tel: "781311030",
    logo: "assets/images/logo.png",
    location: "assets/images/loc.png"
  }

  constructor() {}

}
